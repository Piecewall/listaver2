/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lisenver;

/**
 *
 * @author Orlando
 */
public class NodoLista {
     // 2 campos
    Object Info;
    NodoLista Liga;
   
    //PRIMER CONSTRUCTOR
    public NodoLista(Object dato){
        Info = dato;
        Liga = null;
    }
   
    //SEGUNDO CONSTRUCTOR
    public NodoLista(Object dato, NodoLista liga){
        Info = dato;
        this.Liga = liga;  /////el this solo es para decir que la primera "liga"
    }                          ////es global
}
