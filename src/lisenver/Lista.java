/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lisenver;

/**
 *
 * @author Orlando
 */
public class Lista {
     // declarar los punteros!!!
    NodoLista Primero, ultimo, aux, nuevo, ant, post;
   
    public Lista(){
        Primero = ultimo = aux = nuevo = ant = post = null;
    }
   
    public void insertarAlFrente(Object dato){
        if(Primero==null){// si la lista esta vacia
            Primero = new NodoLista(dato);
            ultimo=Primero;
        }
        else{////ya hay mas nodos en la lista
            nuevo = new NodoLista(dato);
            ultimo.Liga = nuevo;
            ultimo = nuevo;
        }despliegaLista();         
    }
   
    public void insertarAtras(Object dato){
        if(Primero==null){// si la lista esta vacia
            Primero = new NodoLista(dato);
            ultimo=Primero;
        }
        else{
            nuevo = new NodoLista(dato,Primero);
            Primero = nuevo;
        }despliegaLista();
    }   
   
    public void despliegaLista(){
        aux = Primero;
        System.out.println ("##########LISTA COMPLETA#########");
        while (aux != null) {
            System.out.println (aux.Info);
            aux = aux.Liga;
        }
        System.out.println ("##########LISTA COMPLETA#########");
    }
    /////////INSERTAR ANTES DE //////////
    public void insertarAntesDe(Object DatoB,Object DatoI){
        if(Primero==null){
            System.out.println ("lista vacia");
        }
        else{/////hay datos
            if(buscar(DatoB)== true){
                ///////EMPEZAR A REALIZAR EL METODO
                if(aux==Primero){///caso 1
                nuevo= new NodoLista(DatoI,Primero);
                Primero = nuevo;
                }
                else{//en caso de que no este al inicio de la lista
                    nuevo = new NodoLista(DatoI , aux);
                    ant.Liga = nuevo;
                }
            }   
        }
        despliegaLista();
    }
    //////////////INSERTAR DESPUES DE//////////////////
    public void insertarDespuesDe(Object DatoB, Object DatoI){
        if(Primero==null){
            System.out.println ("lista vacia");
        }
        else{///hay metodos
            if(buscar(DatoB)== true){
                //////EMPEZAR A REALIZAR EL METODO/////
                if(aux==Primero){
                    nuevo=new NodoLista(DatoI);
                    Primero.Liga=nuevo;
                }
                else{///en caso de que no este al inicio de la lista
                    nuevo= new NodoLista(DatoI, aux.Liga);
                    aux.Liga=nuevo;
                }
               
            }
        }despliegaLista();
    }
   
    ///////////////METODO ELIMINAR NODO//////////////
    public void eliminarNodo(Object DatoB){
        if(Primero==null){
            System.out.println ("lista vacia");
        }
        else{
            if(buscar(DatoB)==true){///hacer cuatro casos
                if(Primero==ultimo){//// 1 caso
                    Primero=ultimo=null;
                }
                else if(aux==Primero){
                    Primero=aux.Liga;
                    aux=null;
                }
                else if(aux==ultimo){
                    ultimo=ant;
                    ultimo.Liga=null;
                    aux=null;
                }
                else{
                    ant.Liga=aux.Liga;
                    aux=null;
                }
            }
        }despliegaLista();
    }
   
    /////////////METODO PARA SUSTITUIR DATO///////////////
    public void modificaLista(Object DatoB,Object DatoI){
        if(Primero==null){
            System.out.println ("lista vacia");
        }
        else{
            if(buscar(DatoB)==true){
                aux.Info=DatoI;
            }
        }despliegaLista();
    }
    //////////////////////////METODO BUSCAR/////////
    public boolean  buscar(Object DatoB){
        aux = Primero;
        boolean bandera = false;
        while (aux != null && bandera != true) {
            if(DatoB.equals(aux.Info)){// si encuentra el dato
                bandera = true;
            }
            else{//apunta al siguiente nodo
                ant = aux;
                aux = aux.Liga;
                post = aux.Liga;
            }
        }
        if(bandera == true){
            return true;
        }
        else{
            System.out.println ("ese dato no existe");
            return false;
        }
    }
   
}
